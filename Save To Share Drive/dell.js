let launch = async () => {
  let regexStrings = []
  let regexExcludeStrings = []

  //let serviceTagList = ['4dpqx4j', 'FCTCHS1', 'BAD-SERVICE-TAG']
  const os = require('os')
  const fs = require('fs')
  const path = require('path')
  const cwd = process.cwd() // since we are launching from a network drive, but running on a local PC, we need to get the directory the 
                            // where node is executed and use that when requiring dependencies (e.g. puppeteer, csvWriter)
  //const puppeteer = require('puppeteer')
  // using process.cwd() to allow .js file that resides on a network share to require modules installed on local workstation
  const p = path.join(cwd, 'node_modules', 'puppeteer')
  const puppeteer = require(p)

  const browser = await puppeteer.launch({headless: false, args: ['--window-size=1280,960','--disable-infobars']})

  const page = await browser.newPage()
// Display events from page
/*
  page.on('console', (msg) => {
    console.log('Page Console Log:', msg.text)
  })
  page.on('pageerror', (error) => {
    console.log('Page Error', error.message)
  })
  page.on('response', (response) => {
    console.log('Page Response', response.status, response.url)
  })
  page.on('requestfailed', (request) => {
    console.log('Page Request Failure', request.failure().errorText, request.url)
  })
  
  */
  await page._client.send('Emulation.clearDeviceMetricsOverride') // clear default 800x600 viewport size per this post https://github.com/GoogleChrome/puppeteer/issues/1183
  
  let url = `file:${path.join(__dirname, 'index.html')}` // this is coming from same network share directory where .js resides
  // await page.goto(url)
  await page.goto(url, {
    waitUntil: 'networkidle2'
  })

  let appMsg = (text) => {
    console.log('Message from Client Page:', text)  
  }  
  
  let appProcessServiceTags = async (serviceTagList) => {
    await main(browser, os, path, regexStrings, regexExcludeStrings, serviceTagList, cwd)
    console.log('Process Complete!')
    process.exit(0)
  }

  let escapeRegExp = (string) => {
    return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&') // $& means the whole matched string
  }
 
  await page.exposeFunction('appMsg', appMsg)
  await page.exposeFunction('appProcessServiceTags', appProcessServiceTags)

  let filePath = path.join(__dirname, 'regexes.txt')
  console.log('Path for regexes', filePath)
  //let filePath = 'Z:\\My Drive\\Avail Recovery Solutions\\avail-retrieve\\regexes.txt'

  // Retrieve regexes (search strings)
  let f = fs.readFileSync(filePath, {encoding: 'utf-8'}, function(err) { console.log(err) })
  
  // Split string data into array of rows based on OS-specific end of line
  f = f.split(os.EOL)
  f.forEach((regexRow) => {
    let regexStart = 1
    if (regexRow.substring(0, 1) === '-') {
      regexStart = 2 // exclude rule
    } 

    let flagsPosition = regexRow.lastIndexOf('/')
    //console.log('flagsPosition', flagsPosition)
    let regex = regexRow.substring(regexStart, flagsPosition)
    let flags = regexRow.substring(flagsPosition + 1, regexRow.length)
    //console.log('flags', flags)
    //console.log('Adding regex to search array', escapeRegExp(regex))
    if (regexStart === 1) { // include patterns
      regexStrings.push({regex: escapeRegExp(regex), flags: flags})
    } else { // exclude patterns
      regexExcludeStrings.push({regex: escapeRegExp(regex), flags: flags})
    }
  })

  console.log('regexStrings array', regexStrings)
  console.log('regexExcludeStrings array', regexExcludeStrings)

  //var outPath = path.join(__dirname, 'PATH_TO_JSON');
  // Convert object to string, write json to file
  //fs.writeFileSync(outPath, JSON.stringify(json), 'utf8', 
  //    function(err){console.log(err);});
}

let scrape = async (serviceTag, browser, regexStrings, regexExcludeStrings) => { // scrape called by main() for each service tag entered by user
  console.log('in scrape - regexStrings.length', regexStrings.length)
  console.log('in scrape - regexExcludeStrings.length', regexExcludeStrings.length)

  let result = null
  let page = null
  let redirectDetected = false
  try { // using try block so errors such as a detected and canceled redirect can be caught and processing can continue for the next service tag
    page = await browser.newPage()
    let url = 'https://www.dell.com/support/home/us/en/04/product-support/servicetag/' + serviceTag + '/configuration?ref=captchaseen'

    // detect redirection to another page in the case where the service tag is invalid and the website redirects to a product search page
    await page.setRequestInterception(true) // intercept request to detect and cancel redirect - see: https://github.com/GoogleChrome/puppeteer/issues/1132#issuecomment-393724933
    page.on('request', request => { 
      if (request.isNavigationRequest() && request.redirectChain().length) {
        console.log('in page.on "request" - redirect detected - aborting request')
        redirectDetected = true
        request.abort()
      } else {
        request.continue()
      }
    })
  
    await page.goto(url, {
      waitUntil: 'networkidle2'
    })
    // page.evaluate takes a function argument and then arguments to be passed to the function
    result = await page.evaluate(({serviceTag, regexStrings, regexExcludeStrings}) => { // code to execute in page context
      // regexes passed in as an array of regex strings  
      
      //example regexeStrings = [
      //  '/Hard Drive/i',
      //  '/Memory/i',
      //  '/Xeon/i',
      //]
      console.log('in scrape.page.evaluate - regexStrings.length', regexStrings.length)
      console.log('in scrape.page.evaluate - regexExcludeStrings.length', regexExcludeStrings.length)

      let regexes = []
      for (let i = 0; i < regexStrings.length; i++) {
        regexes.push( new RegExp(regexStrings[i].regex, regexStrings[i].flags) )
      }
      
      let regexesExclude = []
      for (let i = 0; i < regexExcludeStrings.length; i++) {
        regexesExclude.push( new RegExp(regexExcludeStrings[i].regex, regexExcludeStrings[i].flags) )
      }

      console.log('regexes.length', regexes.length)
      console.log('regexesExclude.length', regexesExclude.length)

      let regexMatched = (str) => {
        let matched = false
        for (let i = 0; i < regexes.length; i++) {
          if (regexes[i].test(str)) {
            matched = true
            break // break on first matched pattern
          }
        }
        
        return matched
          
      } 

      let regexExcluded = (str) => {
        let excluded = false
        for (let i = 0; i < regexesExclude.length; i++) {
          if (regexesExclude[i].test(str)) {
            excluded = true
            break // break on first matched exclusion pattern
          }
        }
        
        return excluded
          
      }   
      // get data from page elements
      let allComponentData = [] // array that stores part objects of all selected components
      let prodDescr = document.querySelector('#pd-support-banner > div > div > div > div > h1 > span').textContent
      if (prodDescr.substring(0,12) === 'Support for ') { // strip 'Support for ' string, if found
        prodDescr = prodDescr.substring(12, prodDescr.length)
      }

      let container = document.querySelector('#originalConfiguration div.col-lg-12.col-md-12.col-sm-12.hidden-xs')
      if (!container || container.length === 0) return allComponentData
      let hdrElements = container.querySelectorAll('div.top-offset-20') // Select all features
      if (!hdrElements || hdrElements.length === 0) return allComponentData
      for (let element of hdrElements) { // Loop through each component
        let cmpDescrMatch = false // component description match found
        let curComponentData = []  // array for parts objects of the current component that is added to allComponentData array when the component description matches one of the 
                                  // regex expressions or at least one part description of the current component matches one of the regex expressions

        let hdrDescr = element.querySelector('span.show-collapsed').textContent // component description
        // check if component description matches search strings or is exluded by exclude rules
        // Note: Components need to be included in search results even when only the component description is a match - there is additional logic to handle this case just after the
        // partRowElements for loop
        if (regexMatched(hdrDescr)) {
          cmpDescrMatch = true
        }

        if (cmpDescrMatch && regexExcluded(hdrDescr)) { // test for component descr to match an exclude rule (only need to run if component descr matched an include rule)
          cmpDescrMatch = false
        }

        let partRowElements = element.querySelectorAll('tr:nth-child(n+2)') // Select all table rows, except first row which is column titles (i.e selection begins at child 2
                                                                            // because n starts at 0)
        if (!partRowElements || partRowElements.length === 0) continue  // no component parts found, go to next component
        
        for (let row of partRowElements) {
          let partDescrMatch = false
          let dataEl = {}
          dataEl.prodDescr = prodDescr
          dataEl.serviceTag = serviceTag
          dataEl.componentDescr = hdrDescr
          //dataEl.tagname = row.children[0].tagName
          dataEl.partNo = row.children[0].children[0].textContent // each row has one td that contains three divs for displaying part number, qty, and description
          dataEl.partQty = row.children[0].children[1].textContent
          dataEl.partDescr = row.children[0].children[2].textContent
          if (regexMatched(dataEl.partDescr)) { // test for part description match to at least one of the regexes
            partDescrMatch = true
          }

          if (partDescrMatch && regexExcluded(dataEl.partDescr)) { // test for part description to match an exclude rule (only need to run if partDescr matched an include rule)
            partDescrMatch = false
          }

          if (partDescrMatch) curComponentData.push(dataEl) // Push an object with the data onto our array

        } // end for (part table rows)

        if (curComponentData.length === 0 && cmpDescrMatch) { // no parts matched, but component description matched
          let dataEl = {}
          dataEl.prodDescr = prodDescr
          dataEl.serviceTag = serviceTag
          dataEl.componentDescr = hdrDescr
          dataEl.partNo = 'Match found in component description only'
          dataEl.partQty = ''
          dataEl.partDescr = ''
          curComponentData.push(dataEl)
        }

        if (curComponentData.length > 0) { // include component parts data in result set if part description matched an include rule and did not match an exclude rule 
          allComponentData = allComponentData.concat(curComponentData) 
        }
      } // end for (component divs)
      return allComponentData // Return data collected by code running in page context as promise result
    }, {serviceTag, regexStrings, regexExcludeStrings}) // end page.evaluate

  } // end try for extracting data for a particular service tag
  catch (err) { // catch for processing the page request for a particular service tag
    if (redirectDetected) {
      console.error('Web page redirect occurred when processing service tag ' + serviceTag)
      console.error('This could be caused by an invalid service tag. Process will continue with next service tag')
    } else {
      console.error('Error occurred when processing service tag ' + serviceTag + '. Process will continue with next service tag')
      console.error('Error details -', err)
    }
  }
  if (page) await page.close() // close page, if it exists
  return result // Return data to caller as promise result
}

let main = async (browser, os, path, regexStrings, regexExcludeStrings, serviceTagList, cwd) => {
//async function main() {
  try {
    let homeDir = os.homedir()
    // using process.cwd() to allow .js file that resides on a network share to require modules installed on local workstation
    //const c = path.join(process.cwd(), 'node_modules', 'csv-writer') 
    const c = path.join(cwd, 'node_modules', 'csv-writer')
  
//    const createCsvWriter = require('csv-writer').createObjectCsvWriter
    const createCsvWriter = require(c).createObjectCsvWriter

    //let d = new Date().getTime() // UTC Epoch
    //let dt = new Date(d) // UTC Date/Time
    //let df = dt.getUTCFullYear() + '-' + (((dt.getUTCMonth() + 1) < 10) ? '0' : '') + (dt.getUTCMonth() + 1) + '-' + ((dt.getUTCDate() < 10) ? '0' : '') + dt.getUTCDate()
    //df = df + ' ' + dt.getUTCHours() + '-' + dt.getUTCMinutes() + '-' + dt.getUTCSeconds()
    
    let dt = new Date()
    let dtYMD = dt.getUTCFullYear() + '-' + (((dt.getUTCMonth() + 1) < 10) ? '0' : '') + (dt.getUTCMonth() + 1) + '-' + ((dt.getUTCDate() < 10) ? '0' : '') + dt.getUTCDate()
    dtTime = dt.toLocaleTimeString() // replace colon with dash since colon not valid in file name
    dtTime = dtTime.replace(/:/g, '-')
    console.log('formatted time', dtTime)
    df = dtYMD + ' ' + dtTime
    
    // Setup .csv file in document folder on Windows user's home drive and define header row
    const csvWriter = createCsvWriter({
      path: homeDir + '//documents//service-tag-data-' + df + '.csv', // user's Documents folder on Windows
      header: [
        {id: 'serviceTag', title: 'Service Tag'},
        {id: 'prodDescr', title: 'Prod Descr'},
        {id: 'componentDescr', title: 'Component Descr'},
        {id: 'partNo', title: 'Part Num'},
        {id: 'partQty', title: 'Quantity'},
        {id: 'partDescr', title: 'Part Descr'},
      ],
      //append: true
    })
    
    //console.log('in main - regexStrings', regexStrings)
    //console.log('in main - serviceTagList', serviceTagList)
    let allResults = []
    let arrayLength = serviceTagList.length;
    for (let i = 0; i < arrayLength; i++) { // run scrape() for each service tag
  //    let scrape = async (serviceTag, browser, regexStrings) => {

      let curServiceTagData = await scrape(serviceTagList[i], browser, regexStrings, regexExcludeStrings)
      if (curServiceTagData && curServiceTagData.length > 0) { // curServiceTagData could be null if scrape function aborted
        allResults = allResults.concat(curServiceTagData)
      } else {
//        console.log('WARNING: No data found for Service Tag:', serviceTagList[i])
        let dataEl = {}
        dataEl.serviceTag = serviceTagList[i]
        dataEl.prodDescr = ''
        dataEl.componentDescr = '** No data found for service tag **'
        dataEl.partNo = ''
        dataEl.partQty = ''
        dataEl.partDescr = ''
        allResults.push(dataEl)
      }
    } // end for

    //console.log(allResults)
    //csvWriter.writeRecords(records)       // returns a promise
    //.then(() => {
    //  console.log('csvWriter completed write to output file')
    //})
    await csvWriter.writeRecords(allResults)
    console.log('csvWriter completed write to output file')
    await browser.close()
  } catch (err) {
    console.error('Error occurred:', err)
  }
}

// Ready, go!
launch().then(() => {
  console.log('Process Launched!')
})
