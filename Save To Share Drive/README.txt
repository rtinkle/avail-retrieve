Avail Retrieve Application
==========================

Overview:
This application uses Node.js and supporting packages to programmatically control the Chromium browser in order to retrieve Service Tag related information from the Dell Support website.
The controlling program is a custom Node.js program called dell.js and its job is to pass Dell Service Tags and equipment description search strings to the browser. The browser, in turn,
retrieves and filters equipment information for each requested Service Tag. The information retrieved is stored in a .csv file under the user's Documents folder on a Windows PC. 
The .csv file will be named service-tag-data.csv and can be opened directly with Excel. 

As background information, Node.js solutions often rely on pre-built packages in order to get the work done. As mentioned above, this solution
is dependent on a Node.js package called puppeteer and puppeteer is maintained by Google. Node.js solutions are normally stored in a folder called a repository. 
The word repository is term commonly used to refer to a project folder stored in an online Git-based source code control system like GitHub or Bitbucket.

To get things running, you need to install Node.js and then copy the source code from an online repository to a local folder on your PC. Basic installation
and operation instructions are listed below.

Installation:
1. Go to the Node.js website https://nodejs.org and download Node.js for Windows - it is best to download the version designated as LTS (long term stable)
2. Once the download is complete, run the Node.js install package (.msi) from your downloads folder
3. Use File Explorer to create a folder on your PC called avail-retrieve under your Documents folder
4. Download the online repository to your PC by going to the following URL: https://bitbucket.org/rtinkle/avail-retrieve/get/master.zip
5. Use File Explorer to copy the the package.json and package-lock.json files from the root of the embedded folder of the downloaded .zip file to the avail-retrieve folder you created in Step 3 above
6. Open a command window by typing "cmd" in the Windows search box and then clicking "Command Prompt" in the search results
7. In the command window, go to the folder you populated in step 5 directory by entering cd documents\avail-retrieve and pressing <enter>
8. In the command window, install the required Node application packages by entering npm install and pressing <enter>
9. If this is the first version or a new version, copy all files found in the "Save To Share Folder" directory from the downloaded .zip file 
   to a shared folder on your network (e.g. G:\My Drive\Avail Recovery Solutions\avail-retrieve)
10. On your PC, create a shortcut to the dell.bat on the network share - using File Explorer, go to the local documents\avail-retrieve folder and then right-click in the folder
    select New > Shortcut, and then browse to the dell.bat file on the G: share drive (or paste in the path "G:\My Drive\Avail Recovery Solutions\avail-retrieve\dell.bat".  Once the shortcut is created, you can just cl

Operation:
1. Assuming install steps are complete, double-click the dell.bat shortcut in the documents\avail-retrieve directory to launch retrieval process
2. The custom Node.js program will immediately launch a browser page where you can enter a list of Service Tags you want to retrieve from the Dell site
3. Once you enter the service tags, you press the Retrieve button on the web page to initiate the data retrieval process
4. Once the process is finished, it will close the browser it originally opened (assuming everything worked, that is)
5. As mentioned above, you will find the retrieval results in a file called service-tag-data.csv under the user's documents folder
6. Going forward, you can just run the dell command from the avail-retrieve folder whenever you want to run the process

Additional Notes:

The retrieval process uses part description search strings stored in a file called regexes.txt that is in the avail-retrieve folder created above.
This file can be edited to to refine search strings or add more. The search strings are in Regular Expression format. Regular Expressions are a very powerful pattern matching tool
used to search and manipulate textual data. You can read more about Regular Expressions here: https://flaviocopes.com/javascript-regular-expressions/

Listed below are basic regex expression examples that would cause the process to include parts in the retrieval results that had the words 'Hard Drive' or 'Memory' or 'Xeon' in
their description. The characters between the two '/' are the string expressions used to match part descriptions against. The 'i' after the closing '/' is a regular expression flag
used to indicate that the pattern matching is to be done on a case-insensitive basis.

Examples of part descriptions to included in results
====================================================
/Hard Drive/i
/Memory/i
/Xeon/i

You can also indicate that a regex string represents a part description to be excluded from the retrieval results by adding a minus sign ( - ) in front of the expression. For example, 
the expression below would exclude parts that have the word 'Assembly' in their description. This exclusion would occur even if other words in the part description matched based on 
inclusion expressions.

Example of a part description to excluded from results
======================================================
-/Assembly/i
