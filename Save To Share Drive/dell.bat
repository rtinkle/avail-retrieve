echo off
REM This .bat file is set up to be stored on a network share drive, but run on a local PC. This .bat file, in turn, launches a Node.js
REM program (dell.js) that also lives on the share drive, but relies on Node.js and related module dependencies being installed
REM locally on the a user's PC.
REM Since node.js is running on user's pc, it expects modules dependencies to be installed locally in an application specific
REM folder - %homedrive%%homepath%\documents\avail-retrieve\node_modules.
REM The dell.js program residing on the network drive has to use process.cwd() in order to build the path needed to require() locally 
REM installed  modules found in %homedrive%%homepath%\documents\avail-retrieve\node_modules

cd /d %homedrive%%homepath%\documents\avail-retrieve  & :: goto to the avail-retrieve directory on user's home drive
node "G:\My Drive\Avail Recovery Solutions\avail-retrieve\dell.js" & :: run node from user's home drive with a program stored on the share drive
pause